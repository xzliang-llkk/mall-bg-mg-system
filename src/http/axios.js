import axios from "axios";
import { ElNotification } from "element-plus";
import { getToken } from "~/composables/auth";
import { toast } from "~/composables/util";
import store from "~/store"

const service = axios.create({
    baseURL: import.meta.env.VITE_APP_BASE_API,

    timeout: '5000'
})

//请求拦截器
service.interceptors.request.use(
    function (config) {
        // 请求头添加cookie
        const token = getToken()
        if (token) {
            config.headers["token"] = token
            config.headers["token"] = token
        }


        return config;
    },
    function (error) {
        return Promise.reject(error)
    }
)

//响应拦截器
service.interceptors.response.use(
    function (response) {
        return response.data.data
    },
    function (error) {
        const msg = error.response.data.msg || "请求失败"
        //错误提示框
        if (msg == "非法token，请先登录！") {
            store.dispatch("logout").finally(() => {
                location.reload()
            })
        }
        toast(msg, "error")
        return Promise.reject(error)
    }

)

export default service