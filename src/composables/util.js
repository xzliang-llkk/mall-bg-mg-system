/**
 * 封装提示框
 *
 */

import { ElNotification, ElMessage, ElMessageBox } from "element-plus";
import nProgress from "nprogress";


//消息提示
export function toast(message, type = "success", dangerouslyUseHTMLString = true, duration = 3000) {
    ElNotification({
        message,
        type,
        dangerouslyUseHTMLString,
        duration,
    })
}

//提示框
export function showModal(content, type = "warning", title = "警告") {
    return ElMessageBox.confirm(
        content,
        title,
        {
            confirmButtonText: '确定',
            cancelButtonText: '取消',
            type,
        })
}

//显示全屏loading
export function showFullLoading() {
    nProgress.start()
}
// 隐藏全屏loading
export function hideFullLoading() {
    nProgress.done()
}

export function showPrompt(tip, value = '') {
    return ElMessageBox.prompt(tip, '', {
        confirmButtonText: '确认',
        cancelButtonText: '取消',
        inputValue: value
    })
}