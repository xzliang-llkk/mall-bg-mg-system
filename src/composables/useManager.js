/**
 * 
 */
import { ref, reactive } from "vue";
import { updatepassword } from "~/api/manager";
import { showModal, toast } from "~/composables/util";
import { useStore } from "vuex";
import { useRouter } from "vue-router";
import { logout } from "~/api/manager";

//密码
export function useRepassword() {
    const router = useRouter()
    const store = useStore()
    // 修改密码
    const formDrawerRef = ref(null);
    const form = reactive({
        oldpassword: "",
        password: "",
        repassword: "",
    });
    const formRef = ref(null);
    const rules = {
        oldpassword: [
            { required: true, message: "旧密码不能为空", trigger: "blur" },
            // { min: 5, max: 30, trigger: "blur", message: "密码不符合规范" },
        ],
        password: [
            { required: true, message: "新密码不能为空", trigger: "blur" },
            // { min: 5, max: 30, trigger: "blur", message: "密码不符合规范" },
        ],
        repassword: [
            { required: true, message: "确认新密码不能为空", trigger: "blur" },
            // { min: 5, max: 30, trigger: "blur", message: "密码不符合规范" },
        ],
    };
    // 提交
    const onSubmit = () => {
        formRef.value.validate((valid) => {
            if (!valid) {
                return false;
            }
            formDrawerRef.value.showLoading();
            updatepassword(form)
                .then((res) => {
                    toast("修改密码成功，请重新登录");
                    store.dispatch("logout");
                    // 跳转回登录页
                    router.push("/login");
                })
                .catch((err) => {
                    console.log(err);
                })
                .finally(() => {
                    formDrawerRef.value.hideLoading();
                });
        });
    };
    return {
        formDrawerRef,
        form,
        formRef,
        rules,
        onSubmit,
    };
}

//退出登录
export function useLogout() {
    const router = useRouter()
    const store = useStore()
    const handleLogout = () => {
        showModal("是否确定退出登录？").then((res) => {
            logout().finally(() => {
                //清除token、用户状态
                store.dispatch("logout");
                // 跳转回登录页
                router.push("login");
                //提示退出成功
                toast("退出登录成功");
            });
        });
    };
    return { handleLogout }
}