/**
 * 
 */

import { router, addRoutes } from '~/router'
import { getToken } from "~/composables/auth"
import { toast, showFullLoading, hideFullLoading } from "~/composables/util"
import store from './store'

//防止重复加载信息
let hasGetInfo = false
//全局前置守卫
router.beforeEach(async (to, from, next) => {
    //显示loading
    showFullLoading()
    const token = getToken()

    //没有登录强制跳转登录页
    if (!token && to.path != "/login") {
        8
        toast("您没有登录，请先登录谢谢！", 'error')
        return next({ path: "/login" })
    }

    //防止重复登录
    if (token && to.path == "/login") {
        toast("请勿重复登录", 'error')
        return next({ path: from.path ? from.path : '/' })
    }


    //登录时，自动获取用户信息，存储在vuex里
    let hasNewRoutes = false;
    if (token && !hasGetInfo) {
        let { menus } = await store.dispatch("getinfo")
        //防止重复加载信息
        hasGetInfo = true
        //动态路由，用于匹配菜单动态添加路由
        hasNewRoutes = addRoutes(menus)
    }

    //设置页面标题
    let title = (to.meta.title ? to.meta.title : '') + '~商城'
    document.title = title

    //如果有新的路由就跳转指定路由
    hasNewRoutes ? next(to.fullPath) : next()
})


//全局后置守卫
router.afterEach((to, from) => {
    hideFullLoading()
})