import { createApp } from 'vue'

import './style.css'
import ElementPlus from 'element-plus'
import App from './App.vue'
import 'element-plus/dist/index.css'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'
import { router } from './router'
import store from './store'

import 'virtual:windi.css'
import "./permission"

//加载条
import 'nprogress/nprogress.css'

// 自定义指令
import permission from '~/directives/permission'

const app = createApp(App)

//elementIcon全局注册
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}

app
    .use(permission)
    .use(ElementPlus)
    .use(router)
    .use(store)
    .mount('#app')
